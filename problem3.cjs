let carsArray = require('./carsArray.cjs');
function carModelsAlpha(carsArray){
    carsArray.sort(function(a,b){
        var carmodelA = a.car_model.toLowerCase();
        var carmodelB = b.car_model.toLowerCase();
        if(carmodelA < carmodelB){
            return -1;
        }if(carmodelA > carmodelB){
            return 1;
        }else{
            return 0;
        }
    })
    return carsArray;
}
//console.log(carModelsAlpha(carsArray));
module.exports = carModelsAlpha;